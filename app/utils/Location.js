module.exports = {
  kilometersToMiles: function(kilometers) {
    var miles;
    miles = (kilometers * 0.62137);
    miles = Math.round(miles * 100) / 100;
    return miles;
  },
  milesToKilometers: function(miles) {
    var km;
    km = (miles / 0.62137);
    km = Math.round(km * 100) / 100;
    return km;
  },
  deg2rad: function(deg) {
    return deg * (Math.PI / 180);
  },
  getDistanceFromLatLonInMiles: function(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = this.kilometersToMiles(R * c); // Distance in km
    return d.toFixed(2);
  }
}
