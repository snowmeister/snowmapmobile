var moment = require('moment');
module.exports = {
  /*
  Just a utility to get the month and year. At this point, only
  used for the CRIME/POLICE API Calls, but good to keep DATE related methods
  in one place. :-)
  */


  /*
  The Police API expects dates as YYYY-MM format, so this little function
  just grabs todays date and returns the month and year in an array
  RETURNS array(month:integer, year:integer )
  */
  getMonthAndYearFromTodayDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var year = dateObj.getUTCFullYear();
    return [month, year];
  },

  /*
  Due to a quirk/weakness/excuse/bug where the API is updated on the last working day of the month
  AND at time of writing (July 2017) there only seems to be data returned from April and no later,
  this is a nasty hack too! Grab the month and year from our getMonthAndYearFromTodayDate method
  and then build a date to return - this data will be used by the GET request we use to load our
  Police Data from the API
  */
  preparePoliceDate() {
    var dateArray = this.getMonthAndYearFromTodayDate();
    // TODO - Not sure if this is the best way of doing this...
    if (dateArray[0] === 1) {
      dateArray[0] = 10;
    } else {
      dateArray[0] = dateArray[0] - 3;
    }
    if (dateArray[0] < 10) {
      dateArray[0] = '0' + dateArray[0].toString();
    }
    return {
      month: dateArray[0],
      year: dateArray[1]
    };
  },
  formatCrimeDate(datestring){
    var months = new Array();
      months[0] = "January";
      months[1] = "February";
      months[2] = "March";
      months[3] = "April";
      months[4] = "May";
      months[5] = "June";
      months[6] = "July";
      months[7] = "August";
      months[8] = "September";
      months[9] = "October";
      months[10] = "November";
      months[11] = "December";
    var arrCrimeDate = datestring.split('-');
    var month = months[parseInt(arrCrimeDate[1])]
    var year = arrCrimeDate[0];
    return month.toString() + ' ' + year.toString();
  },
  prettifyPPDate(date){
    return moment(date).format('Do MMMM YYYY');
  }
}
