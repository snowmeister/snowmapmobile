var axios = require('axios');
var queryString = require('query-string');
/* See http://postcodes.io/docs for more info about this API */
var postcodeApiUri = 'https://api.postcodes.io/postcodes'
module.exports = {

  /*
  Sidestepping the whole validation thing here. Why make things
  harder than they need to be? Use the Postcodes.io API - job done
  */
  validatePostcode(postcode) {
    var encodedURI = window.encodeURI(postcodeApiUri +'/' +postcode + '/validate');
    return axios.get(encodedURI).then(function(response) {
      return response.data.result
    })
    .catch(function(error) {
      // TODO - Lets do this more elegantly!
      console.error(error);
    });
  },

  /*
  Assuming we have a valid postcode (i.e: validated BEFORE being passed in here -
  not 100% certain this is the best approach however..)
  This method fires the postcode at the API and gets the returned Lat/Lng
  */
  getLatLngFromPostcode(postcode) {
    var encodedURI = window.encodeURI(postcodeApiUri +'/'+ postcode);
    return axios.get(encodedURI)
      .then(function(response) {
        var latitude = response.data.result.latitude;
        var longitude = response.data.result.longitude;
        var postcodeData = response.data.result;
        return [latitude, longitude, postcodeData];
      })
      .catch(function(error) {
        // TODO - Lets do this more elegantly!
        console.error(error);
      });
  },
  getPostcodeFromLatLng(lat, lng){
    // We need to fire the Lat/Lng at the postcodes API to be able to get our postcode
    var encodedURI = window.encodeURI(postcodeApiUri + '?lon='+ lng+'&lat='+ lat);
    return axios.get(encodedURI)
      .then(function(response) {
        return response.data.result
      })
      .catch(function(error) {
        // TODO - Lets do this more elegantly!
        console.error(error);
      });
  },
  getNearbyPostCodes(postcode){
      var qs = queryString.parse(window.location.search)
      var postcode = qs.postcode;
      var encodedURI = window.encodeURI(postcodeApiUri +'/' +postcode + '/nearest');
      return axios.get(encodedURI)
        .then(function(response) {
          return response.data.result.map(function(item){
            return item.postcode
          }).join(',')
        })
        .catch(function(error) {
          // TODO - Lets do this more elegantly!
          console.error(error);
        });
  }

}
