var queryString = require('query-string');
var axios = require('axios');

var proxy = 'https://aroundme.snowmeister.co.uk/proxy.php?url='
module.exports = {
  getBroadbandData(){
    var qs = queryString.parse(window.location.search)
    var url = 'https://map.snowmeister.co.uk/broadband/'+ qs.postcode.replace(' ','');
    var remoteurl = proxy + url
    return axios.get(remoteurl).then(function(response){
      return response
    })
  },

  getCourtsData(){
    var qs = queryString.parse(window.location.search)
    var url = 'https://map.snowmeister.co.uk/courts/'+ qs.postcode.replace(' ','');
    var remoteurl = proxy + url
    return axios.get(remoteurl).then(function(response){

      // console.dir(response)
      return response
    })
  },
  getSchoolsData(){
    var qs = queryString.parse(window.location.search)
    var url = 'https://map.snowmeister.co.uk/schools/'+ qs.postcode;
    var remoteurl = proxy + url
    return axios.get(remoteurl).then(function(response){
      console.dir(response.data.schools);
      return response.data.schools
    })
  },
  processPPData(ppdata){
    return ppdata.map(function(item){
      return {
        price: item.price,
        tui: item.tui,
        paon: item.paon,
        street: item.street,
        oldNew: item.oldNew,
        propertyType: item.propertyType,
        duration: item.duration,
        dateOfTransfer: new Date(item.dateOfTransfer)
      }
    })
  },
  getPPData(strpostcodes){
      var url = proxy + window.encodeURI('http://ppapi.snowmeister.co.uk/pricepaid/'+ strpostcodes);
      var encodedURI = window.encodeURI(url);
        return axios.get(encodedURI).then(function(response){
          console.clear()
          console.dir(response)
          return this.processPPData(response.data)
      }.bind(this))
  }
}
