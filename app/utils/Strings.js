module.exports = {
  toProperCase(arr){
    return arr.map(function(word){
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }).join(' ');
  },
  titleFromSeparatedString(str, separator){
    return this.toProperCase(str.split(separator));
  },
  sanitizeUrl(url){
    var sane = url.replace(/^https?\:\/\//i, "");
    return sane;
  },
  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
