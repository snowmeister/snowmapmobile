var axios = require('axios');
var dateutils = require('./Date');
/* See https://data.police.uk/docs/ for the lowdown on this Data and API! */
var policeApiUri = 'https://data.police.uk/api/crimes-street/all-crime';
//var policeApiUri = 'https://data.police.uk/api/';

module.exports = {
  /*
  At present, only street level crimes, but there are a few other API methods
  that could provide some interesting data...
  */
  getStreetLevelCrimes(lat, lng) {
    var crimedate = dateutils.preparePoliceDate();
    var encodedURI = window.encodeURI(policeApiUri+'?lat='+lat +'&lng='+lng +'&date='+crimedate.year+'-'+crimedate.month);
    return axios.get(encodedURI).then(function(response) {
      return response.data
    })
    .catch(function(error) {
      return error;
    });
  }
}
