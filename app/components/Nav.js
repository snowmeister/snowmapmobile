var React = require('react');
var ReactDom = require('react-dom');
var NavLink = require('react-router-dom').NavLink;
class Nav extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      showNav: false
    }
    this.toggleNav = this.toggleNav.bind(this);
  }
  toggleNav(){
    var showNav = this.state.showNav;
    showNav = showNav ? false : true;
    this.setState(function(){
      return {
        showNav: showNav
      }
    })
  }
  render(){
    return(
      <div className='container'>
      <div className='navbar navbar-default navbar-fixed-top'>
        <div className='navbar-header'>
          <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar' onClick={this.toggleNav}>
                  <span className='sr-only'>Toggle navigation</span>
                  <span className='icon-bar'></span>
                  <span className='icon-bar'></span>
                  <span className='icon-bar'></span>
                </button>

          <NavLink className='navbar-brand' exact to='/'>AroundMe</NavLink>
        </div>
        <div id='navbar' className={ (this.state.showNav ? '' : 'collapse') + ' navbar-collapse'}>
          <ul className='nav navbar-nav'>
            <li>
              <NavLink exact to='/' onClick={this.toggleNav}>Home</NavLink>
            </li>
            <li>
              <NavLink to='/about' onClick={this.toggleNav}>About</NavLink>
            </li>
          </ul>
        </div>
        </div>
      </div>
    )
  }
}
module.exports = Nav;
