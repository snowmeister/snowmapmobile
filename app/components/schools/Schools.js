var React = require('react');
var PropTypes = require('prop-types');
var Loading = require('../Loading');
var queryString = require('query-string');
var uuidv1 = require('uuid/v1');
var data = require('../../utils/Data');
var stringUtils = require('../../utils/Strings');
var locationUtils = require('../../utils/Location');


// TODO - This MUST be moved...KEEP IT DRY See CrimeItem.CrimeDistance
function SchoolDistance(props){
  var lat = props.ulat;
  var lng = props.ulng;
  var clat = props.clat;
  var clng = props.clng;
  var distance = locationUtils.getDistanceFromLatLonInMiles(lat, lng, clat, clng);
  return (
    <span>
      <strong>{distance} miles</strong> from you
    </span>
  )
}


function SchoolItem(props){
  var school = props.school.school
  var lat = props.lat
  var lng = props.lng
  var url = school.Website_address
  var saneLink = stringUtils.sanitizeUrl(school.Website_address)


  return(
    <div className='school-item'>
      <div className='panel panel-default'>
        <div className='panel-heading'>{school.Establishment_name}</div>
        <div className='panel-body'>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Distance: <SchoolDistance ulat={lat} ulng={lng} clat={school.Latitude} clng={school.Longitude}/>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Age Range:<strong>{school.Statutory_lowest_age}-{school.Statutory_highest_age}yrs </strong>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Type of school:<strong>{school.Type_of_establishment}</strong>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Gender: <strong>{school.Gender}</strong>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
          Religious character: <strong>{school.Religious_character}</strong>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Admissions Policy: <strong>{school.Admissions_policy}</strong>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Boarders: <strong>{school.Boarders}</strong>
          </div>
          <div className='col-xs-12 col-sm-6 col-md-6'>
            Headteacher: <strong>{school.Headteacher}</strong>
          </div>
            {url.length > 0 &&

                 <div className='col-xs-12 col-sm-6 col-md-6'>
                 Website: <strong><a target='_blank' href={'https://'+ saneLink}>{saneLink} <i className='fa fa-external-link fa-fw'></i></a></strong>
                </div>

            }
        </div>

      </div>

    </div>
  )
}
SchoolItem.propTypes = {
  school: PropTypes.object.isRequired,
  lat: PropTypes.string.isRequired,
  lng: PropTypes.string.isRequired,

}
class Schools extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      data: null,
      lat: null,
      lng: null,
      postcode: null
    }
  }
  componentDidMount(){
    var query = queryString.parse(this.props.location.search)
    data.getSchoolsData().then(function(response){
      this.setState(function(){
        return {
          loading: false,
          data: response,
          lat: query.lat,
          lng: query.lng,
          postcode: query.postcode

        }
      })
    }.bind(this))
  }
  render() {
    var s = this.state;
    var schools = s.data;

    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className='panel panel-default'>
              <div className='panel-body clearfix'>
                <h1>Schools</h1>
                  {s.loading && <Loading/>}
                  {!s.loading &&
                    <div>

                    <p>Found {s.data.length} schools in your area.</p>
                    {schools.map(function(school){
                    return  <SchoolItem key={uuidv1()} school={school} lat={s.lat} lng={s.lng}></SchoolItem>
                    })}
                  </div>
                  }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
Schools.propTypes = {}

module.exports = Schools;
