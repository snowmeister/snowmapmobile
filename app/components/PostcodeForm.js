var React = require('react');
var PropTypes = require('prop-types');
var postcodes = require('../utils/Postcodes');
class PostcodeForm extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      postcode:  ''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  updatePostcodeState(postcode){
    this.setState(function(postcode){
      return {
        postcode:postcode
      }
    })
  }
  handleSubmit(event){
    event.preventDefault();
    postcodes.validatePostcode(this.state.postcode)
    .then(function(validPostcode){
      if(validPostcode){
        return this.props.locationHandler(this.state.postcode)
      }else{
        // TODO We need to handle this error!
      }
    }.bind(this));
  }
  handleChange(event){
    var value = event.target.value;
    return this.setState(function(){
      return {
        postcode:value
      }
    })
  }
  render(){
    return (<form className='form form-inline'>
      <div className='form-group'>
      <label htmlFor='postcode-input'>{this.props.label} </label>
      <input
        id='postcode-input'
        type='text'
        className='form-control'
        placeholder={this.props.placeholder}
        value={this.state.postcode}
        onChange={this.handleChange}
      />
      <button className='btn btn-success' type='button' onClick={this.handleSubmit} disabled={this.state.postcode.length < 4}><i className="fa fa-fw fa-check"></i></button>
      <button className='btn btn-default' type='button' onClick={this.props.resetHandler}><i className="fa fa-fw fa-times"></i></button>
      </div>
    </form>)
  }
}
PostcodeForm.propTypes = {
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  resetHandler: PropTypes.func.isRequired,
  locationHandler: PropTypes.func.isRequired
}
PostcodeForm.defaultProps = {
  label: 'Please enter your postcode',
  placeholder: 'eg: DA1 3EF',
  resetHandler: null,
  locationHandler: null
}
module.exports = PostcodeForm;
