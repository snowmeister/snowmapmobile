var React = require('react');
var PropTypes = require('prop-types');

class Howdy extends React.Component {
  render(){
    var doingLocate = this.props.doingLocate;
    return(
      <div className='panel-body'>
      <h1>Howdy!</h1>
      <p>This app allows you to use your current location (or a postcode), to find useful information about your area.</p>
        <button className='btn btn-success' onClick={this.props.useLocationHandler}>{!doingLocate && <span><i className="fa fa-location-arrow fa-fw"></i> Use my location</span>}{doingLocate && <span><i className="fa fa-spinner fa-spin fa-fw"></i> Just a moment</span> }</button>
        {!doingLocate &&
          <button className='btn btn-default' onClick={this.props.usePostcodeHandler}>
            <i className="fa fa-envelope-o fa-fw"></i> Explore a postcode
          </button>
        }
        </div>
    )
  }
}
Howdy.propTypes = {
  useLocationHandler: PropTypes.func.isRequired,
  usePostcodeHandler: PropTypes.func.isRequired,
  doingLocate: PropTypes.bool.isRequired
}

module.exports = Howdy;
