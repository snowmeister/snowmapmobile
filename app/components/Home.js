/*
TODO: Things that I reckon should be refactored.. -

1 - It would be good to be able to pass an array of button definition objects into the
Alert component, including event handlers etc. IS this overkill? Is this the REACT WAY? One to investigate...

*/

var React = require('react');
var ReactDom = require('react-dom');
var ReactRouter = require('react-router-dom');
var PropTypes = require('prop-types');
var PostcodeForm = require('./PostcodeForm');
var Alert = require('./Alert');
var Howdy = require('./Howdy');
var postcodes = require('../utils/Postcodes');
var NavLink = require('react-router-dom').NavLink;
var Redirect = require('react-router-dom').Redirect;
class Home extends React.Component{
  constructor(props){
    super(props)
    this.state ={
      location: [],
      usepostcode: false,
      userlatlng: [],
      postcodeSuggestions: [],
      postcode: '',
      doingLocate: false,
      canLocate: true,
      hasData: false,
    }
    this.togglePostcode = this.togglePostcode.bind(this);
    this.getUserLocationFromPostcode = this.getUserLocationFromPostcode.bind(this);
    this.getUserLocation = this.getUserLocation.bind(this);
    this.setPostcodeAndLatLng = this.setPostcodeAndLatLng.bind(this);
  }
  /*
  TODO: Looking at this, I'm not really convinced this is the best place for this
  particular piece of functionality...I suspect this will need to be moved out to be DRY
  */
  getUserLocationFromPostcode(postcode){
    return postcodes.getLatLngFromPostcode(postcode).then(function(locationdata){
      localStorage.setItem('snowmapdata.lat', locationdata[0])
      localStorage.setItem('snowmapdata.lng', locationdata[1])
      localStorage.setItem('snowmapdata.postcode', postcode)

      this.setState(function(){
        return{
          userlatlng: locationdata,
          postcode: postcode
        }
      })
    }.bind(this))
  }
  getUserLocation(){
    if (navigator.geolocation) {
      this.setState(function(){ return {doingLocate: true}});
      return navigator.geolocation.getCurrentPosition(function(position) {
        // Get the coordinates of the current position.
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        localStorage.setItem('snowmapdata.lat', lat);
        localStorage.setItem('snowmapdata.lng', lng);

        var postcode = postcodes.getPostcodeFromLatLng(lat, lng).then(function(postcodes) {
          this.setState(function(){
            return {
              doingLocate: false,
              postcodeSuggestions: postcodes,
              location: [lat, lng]
            }
          });
        }.bind(this))
      }.bind(this));
    } else {
      // No geolocation available
      this.setState(function(){
        return {
          canLocate: false
        }
      });
    }
  }
  togglePostcode(event){
    if(event){
      event.preventDefault();
    }
    var postcodevalue = this.state.usepostcode;
    // Toggle the postcodevale and update the state
    postcodevalue = postcodevalue ? false : true;
    return this.setState(function(){
      return {
        usepostcode: postcodevalue
      }
    })
  }
  setPostcodeAndLatLng(event){
    var lat = this.state.location[0];
    var lng = this.state.location[1];
    var postcode = event.target.value;
    this.setState(function(){
      var postcodeObj = this.state.postcodeSuggestions.filter(function(item){
        return item.postcode === postcode
      })
        localStorage.setItem('snowmapdata.postcode', postcodeObj[0].postcode);
      return {
        userlatlng: [lat,lng,postcodeObj[0].postcode]
      }
    })
  }
  componentDidMount(){
    var lslat, lslng, lspostcode;
    lslat = localStorage.getItem('snowmapdata.lat');
    lslng = localStorage.getItem('snowmapdata.lng');
    lspostcode = localStorage.getItem('snowmapdata.postcode');

  }
  render(){
    var location = this.state.location;
    var usepostcode = this.state.usepostcode;
    var userlatlng = this.state.userlatlng;
    var doingLocate = this.state.doingLocate;
    var postcodeSuggestions = this.state.postcodeSuggestions;


    var lslat, lslng, lspostcode;
    lslat = localStorage.getItem('snowmapdata.lat');
    lslng = localStorage.getItem('snowmapdata.lng');
    lspostcode = localStorage.getItem('snowmapdata.postcode');
    return(
      <div className='container'>
        <div className='row'>
          <div className='col-xs-12'>
            <div className='panel panel-default'>
               {(lslat !== 'null') && (lslng !== 'null') && (lspostcode !== 'null') &&
                <Redirect to={'/main?lat='+lslat+'&lng='+lslng+'&postcode='+ lspostcode}/>
               }
            {userlatlng.length < 1 && postcodeSuggestions.length === 0 && !usepostcode &&
              <Howdy useLocationHandler={this.getUserLocation} usePostcodeHandler={this.togglePostcode} doingLocate={doingLocate}/>
            }
            {usepostcode && userlatlng.length === 0 &&
              <div className='panel-body'>
              <h1>Postcode Explorer</h1>
              <p>Please enter the postcode you want to use as the location to explore...</p>
              <PostcodeForm label='Postcode' resetHandler={this.togglePostcode} locationHandler={this.getUserLocationFromPostcode} placeholder='eg: DA1 1UX' />
              </div>
            }
            {usepostcode && userlatlng.length > 0 &&
              <div className='panel-body'>
              <Alert title='Success' alertType='alert-success'>
                <p>Cool, the Postcodes.io API has returned a Lat/Lng for your Postcode, and indicates you are somehwere around <strong>{this.state.userlatlng[2].admin_ward}, {this.state.userlatlng[2].admin_district}</strong> </p>
                <p>Latitude: <strong>{userlatlng[0]}</strong>, Longitude: <strong>{userlatlng[1]}</strong></p>
                <div className='text-right push-down'>
                <NavLink className='btn btn-success' to={'/main?lat='+userlatlng[0] +'&lng='+userlatlng[1]+'&postcode='+this.state.postcode}>All looks good, proceed...</NavLink>
                <button className='btn btn-default'>Refine location</button></div>
              </Alert>
              </div>
            }
            {postcodeSuggestions.length > 0 && userlatlng.length === 0 &&
              <div className='panel-body'>
              <Alert title='Success' alertType='alert-success'>
                <p>Cool, we have found your location. To continue, please select your postcode from the list below. If you are unsure, just select the first in the list.</p>
              </Alert>
              <ul className='list list-unstyled'>
              {postcodeSuggestions.map(function(item){
                return <li key={item.postcode}>
                  <input type='button' onClick={this.setPostcodeAndLatLng} className='btn btn-default btn-full-width' value={item.postcode}/>
                  </li>
              }.bind(this))}
              </ul>
              </div>
            }
            {postcodeSuggestions.length > 0 && userlatlng.length > 0 &&
              <Redirect to={'/main?lat='+userlatlng[0]+'&lng='+ userlatlng[1]+'&postcode='+ userlatlng[2]}/>
            }
          </div>
        </div>
      </div>
      </div>
    )
  }
}
// TODO - Do we need this? Leave it in place for the time being dood, there is a good chance we will need to extend this further...
Home.propTypes = {}
Home.defaultProps = {}
module.exports = Home;
