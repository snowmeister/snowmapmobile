var React = require('react');
var PropTypes = require('prop-types');
// Using UUID to ensure unique IDs on keys for repeating elements
var uuidv1 = require('uuid/v1');
var stringUtils = require('../../utils/Strings');

/*
At present, this is a stateless function component, this may change.
Also this may need decoupling and moving out into its onw component
*/
function FilterCrimeComboItem(props){
  var crimeTitle  = stringUtils.titleFromSeparatedString(props.category, '-')
  return(
    <option id={uuidv1()}
      value={props.category}>
        {crimeTitle} - {props.count}
    </option>
  )
}
FilterCrimeComboItem.propTypes = {
  category: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
}

class FilterCrimeCombo extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      crimes: props.crimes,
      selectedItem: props.selectedItem
    }
  }
  render(){
    var crimes = this.state.crimes.reduce(function(counts,crime){
      counts[crime.category] = (counts[crime.category] || 0) + 1;
      return counts;
    },{});
    var changehandler = this.props.changehandler
    // TODO - Not 100% certain this is the best way of doing this...investigate further...
    return (<select className='form-control' id='crime-type-picker' value={this.props.selectedItem} onChange={changehandler}>
      <FilterCrimeComboItem category='All' count={this.state.crimes.length} key={uuidv1()}/>
      {Object.keys(crimes).map(function(crime){
        return (
          <FilterCrimeComboItem category={crime} count={crimes[crime]} key={uuidv1()}/>
        )
      })}
    </select>)
  }
}
FilterCrimeCombo.propTypes = {
  crimes: PropTypes.array.isRequired,
  changehandler: PropTypes.func.isRequired,
  selectedItem: PropTypes.string.isRequired
}
FilterCrimeCombo.defaultProps = {
  crimes: [],
  selectedItem:'All'
}
module.exports = FilterCrimeCombo;
