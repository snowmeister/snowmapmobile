var React = require('react');
var PropTypes = require('prop-types');

var Crimeitem = require('./Crimeitem');
// Using UUID to ensure unique IDs on keys for repeating elements
var uuidv1 = require('uuid/v1');

function CrimeList(props) {

    var crimes = props.crimes;
    return (
      <ul className='list list-inline'>
      {crimes.map(function(crime){

        return (
          <Crimeitem key={uuidv1()} crime={crime} lat={props.lat} lng={props.lng} />
        )
      })}
      </ul>
    )
}
CrimeList.propTypes = {
  crimes: PropTypes.array.isRequired,
  lat: PropTypes.string.isRequired,
  lng: PropTypes.string.isRequired
}

module.exports = CrimeList;
