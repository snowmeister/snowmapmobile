var React = require('react');
var queryString = require('query-string');
var PropTypes = require('prop-types');
var FilterCrimeCombo = require('./FilterCrimeCombo');
var CrimeList = require('./CrimeList');
var Loading = require('../Loading');
var police = require('../../utils/Police');
var dateutils = require('../../utils/Date');
var stringUtils = require('../../utils/Strings');
var Alert = require('../Alert');
// Small component to show the current selected results status
class CurrentListStatus extends React.Component{
  render(){
    var crimeTitle  = stringUtils.titleFromSeparatedString(this.props.catName, '-')
    if(this.props.catName === 'All'){
      return (
        <span> <strong>{this.props.count}</strong> street level crimes</span>
      )
    }else{
      return(
        <span> <strong>{this.props.count}</strong> incidents of <strong>{crimeTitle}</strong></span>
      )
    }
  }
}

CurrentListStatus.propTypes = {
  count: PropTypes.number.isRequired,
  catName: PropTypes.string.isRequired
}
class Crime extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crimes: null,
      loading: true,
      currentdata: null,
      selectedCrimeCategory: 'All',
      ajaxerror:false,
      errorobj: null,
      loadingdata: true,
      lat:null,
      lng:null
    }
    this.filterCrimesByType = this.filterCrimesByType.bind(this);
  }
  filterCrimesByType(event) {
    var category = event.target.value
    var crimes = this.state.crimes;
    if (category === 'All') {
      this.setState(function() {
        return {
          currentdata: crimes,
          selectedCrimeCategory:  'All'
        }
      })
    } else {
      var filtered = crimes.filter(function(crime) {
        return crime.category === category
      })
      return this.setState(function(){
        return {
          currentdata : filtered,
          selectedCrimeCategory: category
        }
      })
    }
  }
  componentDidMount() {
    var query = queryString.parse(this.props.location.search)
    return police.getStreetLevelCrimes(query.lat, query.lng).then(function(data) {
      // TODO - investigate the try/catch here, as really not sure this is the best approach!
      if(data.message){
        this.setState(function() {
          return {ajaxerror: true, errorobj: data, loading:false, lat: query.lat, lng:query.lng}
        })
      }else{
        this.setState(function() {
          return {crimes: data, loading: false, currentdata: data, lat: query.lat, lng:query.lng}
        })
      }
    }.bind(this)); // Make sure we preserve the context of 'this'...
  }
  render() {
    var crimes = this.state.currentdata;
    var changehandler = this.filterCrimesByType;
    var ajaxerror = this.state.ajaxerror;
    var loading = this.state.loading;
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className='panel panel-default'>
              <div className='panel-body clearfix'>
                <h1>Local Crime</h1>
                {loading === true &&
                 <Loading></Loading>
                }
                {ajaxerror &&
                  <Alert alertType='alert-danger' title='Sorry'>
                    <p>Oops, it seem seems something has gone wrong while loading the crime data from the Police API. The error is shown below</p>
                    <code className='code push-down'>
                      {this.state.errorobj.message}
                    </code>
                  </Alert>
                }
                {crimes !== null && <p>The UK Police API has returned
                  <CurrentListStatus count={crimes.length} catName={this.state.selectedCrimeCategory}/> within an approximate 1 mile radius of your location.</p>
                }
                {crimes !== null && <div className="row clearfix">
                  <div className='col-xs-12 col-sm-6 col-md-6'>
                    <form className='form form-inline'>
                      <div className='form-group'>
                        <label htmlFor='crime-type-picker'>Filter by type:
                        </label>
                        <FilterCrimeCombo crimes={crimes} changehandler={changehandler} selectedItem={this.state.selectedCrimeCategory} />
                      </div>
                    </form>
                  </div>
                  <div className='col-xs-12'>
                    <hr/>
                    <CrimeList crimes={crimes} lat={this.state.lat} lng={this.state.lng}/>
                  </div>
                </div>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
Crime.propTypes = {}

module.exports = Crime;
