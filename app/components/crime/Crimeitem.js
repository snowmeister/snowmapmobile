var React = require('react');
var PropTypes = require('prop-types');
var uuidv1 = require('uuid/v1');
var stringUtils = require('../../utils/Strings');
var dateUtils = require('../../utils/Date');


var locationUtils = require('../../utils/Location');

// TODO - This MUST be moved...KEEP IT DRY see Schools.SchoolDistance
function CrimeDistance(props){
  var lat = props.ulat;
  var lng = props.ulng;
  var clat = props.clat;
  var clng = props.clng;
  var distance = locationUtils.getDistanceFromLatLonInMiles(lat, lng, clat, clng);
  return (
    <p>Approx <strong>{distance} miles</strong> from you</p>
  )
}

class Crimeitem extends React.Component {
  prepareOutCome(outcome){

    if(outcome === null){
      return 'No outcome available'
    }else{
      return outcome.category
    }
  }
  render(){
    var crime = this.props.crime;
    var title = stringUtils.titleFromSeparatedString(crime.category, '-')
    var street = crime.location.street.name;
    var datestring = dateUtils.formatCrimeDate(crime.month);
    var clat = crime.location.latitude;
    var clng = crime.location.longitude;
    var lat = this.props.lat;
    var lng = this.props.lng;
    var outcome_status = this.prepareOutCome(crime.outcome_status);
    return(
      <li key={uuidv1()} className='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
        <div className='panel panel-default'>
          <div className='panel-heading'>{title}</div>
          <div className='panel-body'>
            <div className='col-xs-12 col-sm-6'><p>When: <strong>{datestring}</strong></p></div>
            <div className='col-xs-12 col-sm-6'><CrimeDistance ulat={lat} ulng={lng} clat={clat} clng={clng}/></div>
            <div className='col-xs-12'><p>Where: <strong>{street}</strong></p></div>
            <div className='col-xs-12'><p>Outcome: <strong>{outcome_status}</strong></p></div>
          </div>
        </div>
      </li>
    )
  }
}
Crimeitem.propTypes = {
  crime: PropTypes.object.isRequired,
  lat: PropTypes.string.isRequired,
  lng: PropTypes.string.isRequired
}
module.exports = Crimeitem;
