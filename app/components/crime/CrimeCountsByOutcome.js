var React = require('react');
var PropTypes = require('prop-types');
// Using UUID to ensure unique IDs on keys for repeating elements
var uuidv1 = require('uuid/v1');
var stringUtils = require('../utils/Strings');
/*
At present, this is a stateless function component, this may change.
Also this may need decoupling and moving out into its onw component
*/
function CrimeCountItem(props){
  var crimeTitle  = stringUtils.titleFromSeparatedString(props.outcome, '-')
  return(
    <option id={uuidv1()} value={crimeTitle}>{crimeTitle} - {props.count}</option>
  )
}
CrimeCountItem.propTypes = {
  outcome: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
}

class CrimeCountsByOutcome extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      crimes: props.crimes
    }
  }
  render(){
    var crimes = this.state.crimes.reduce(function(counts,crime){
      if(crime.outcome_status !== null){
        counts[crime.outcome_status.category] = (counts[crime.outcome_status.category] || 0) + 1;
      }else{
        counts['none available'] = (counts['none available'] || 0) + 1;

      }
      return counts;
    },{});
    // TODO - Not 100% certain this is the best way of doing this...investigate further...
    return (<select className='form-control'>
      <option id={uuidv1()} value='All'>All - {this.state.crimes.length}</option>
      {Object.keys(crimes).map(function(crime){
        return (
          <CrimeCountItem outcome={crime} count={crimes[crime]} key={uuidv1()}/>
        )
      })}
    </select>)
  }
}
CrimeCountsByOutcome.propTypes = {
  crimes: PropTypes.array.isRequired
}
CrimeCountsByOutcome.defaultProps = {
  crimes: []
}
module.exports = CrimeCountsByOutcome;
