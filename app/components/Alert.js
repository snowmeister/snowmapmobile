var React = require('react');
var PropTypes = require('prop-types');
/*
TODO - Lets have the ability to throw an array of button objects in here please. AND, what about dismissable? or timed alerts? Sort it out!
*/
class Alert extends React.Component {
  render(){
    return(
      <div className={'alert ' + this.props.alertType}>
        <h2>{this.props.title}</h2>
        {this.props.children}
      </div>
    )
  }
}
Alert.propTypes = {
  alertType: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

module.exports = Alert;
