var React = require('react');
var PropTypes = require('prop-types');
var uuidv1 = require('uuid/v1');

class CourtItem extends React.Component {
  constructor(props){
    super(props)

  }
  render(){
    var court = this.props.court;
    return(
      <div className='panel panel-default'>
        <div className='panel-heading'>
          {court.name}
        </div>
        <div className='panel-body'>

          <div className="col-xs-12 col-sm-12 col-md-6">
            <h5>Address</h5>
            <ul>
            {court.address.address_lines.map(function(addressline)
              {
              return  <li key={uuidv1()}>{addressline}</li>
            })}
              <li key={uuidv1()}>{court.address.town}</li>
              <li key={uuidv1()}>{court.address.county}</li>
              <li key={uuidv1()}>{court.address.postcode}</li>
            </ul>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-6">
            {court.types.length > 0 &&
              <div>
                <h5>Type of court</h5>
                <ul className='list list-inline'>
                  {court.types.map(function(courtType){
                    return <li key={uuidv1()}><span className='label label-primary'>{courtType}</span></li>
                  })}
                </ul>
              </div>
            }

            <h5>Areas of Law</h5>
            <ul className='list list-inline'>
              {court.areas_of_law.map(function(item)
                {
                return  <li key={uuidv1()}><span className='yes-icon'><i className="fa fa-fw fa-check"></i></span> {item}</li>
              })}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}
CourtItem.propTypes = {
  court: PropTypes.object.isRequired
}

module.exports = CourtItem;
