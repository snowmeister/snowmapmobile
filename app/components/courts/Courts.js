var React = require('react');
var Loading = require('../Loading');
var axios = require('axios');
var data = require('../../utils/Data');
var queryString = require('query-string');
var uuidv1 = require('uuid/v1');
var CourtItem = require('./CourtItem');
class Courts extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      loading: true,
      data: null,
      lat: null,
      lng: null,
      postcode: null
    }
  }
  componentDidMount(){
    var query = queryString.parse(this.props.location.search)
    data.getCourtsData().then(function(response){
      console.dir(response.data)
      this.setState(function(){
        return {
          loading: false,
          data: response.data.results,
          lat: query.lat,
          lng: query.lng,
          postcode: query.postcode

        }
      })
    }.bind(this))
  }
  render(){
    var s = this.state;

    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className='panel panel-default'>
              <div className='panel-body clearfix'>
                <h1>Courts</h1>
                {s.loading &&
                  <Loading />
                }
                {!s.loading && s.data !== null &&
                  <div>
                    <div className="col-xs-12 col-sm-12">
                      <p>Found <strong>{s.data.length} courts</strong> in your region</p>
                      {s.data.map(function(court){
                         return <CourtItem key={uuidv1()} court={court.item.data} lat={s.lat} lng={s.lat}></CourtItem>
                      })}
                    </div>
                  </div>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

module.exports = Courts;
