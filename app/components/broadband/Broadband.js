var React = require('react');
var Loading = require('../Loading');
var axios = require('axios');
var data = require('../../utils/Data');
var queryString = require('query-string');

class Broadband extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      loading: true,
      data: null
    }
  }
  componentDidMount(){
    data.getBroadbandData().then(function(response){
      this.setState(function(){
        return {
          loading: false,
          data: response
        }
      })
    }.bind(this))
  }
  render(){
    var s = this.state;
    console.dir(s);
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className='panel panel-default'>
              <div className='panel-body clearfix'>
                <h1>Broadband</h1>
                {s.loading &&
                  <Loading />
                }
                {!s.loading && s.data !== null &&
                  <div>
                    <div className="col-xs-12 col-sm-12 col-md-6">
                      <h3>Download Speeds</h3>
                      <p>Min Download Speed: <strong>{s.data.Min_download_speed_Mbits} Mbit/s</strong></p>
                      <p>Max Download Speed: <strong>{s.data.Max_download_speed_Mbits} Mbit/s</strong></p>
                      <p>Avg Download Speed: <strong>{s.data.Avg_download_speed_Mbits} Mbit/s</strong></p>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-6">
                      <h3>Upload Speeds</h3>
                      <p>Min Upload Speed: <strong>{s.data.Min_ul_spd_Mbits} Mbit/s</strong></p>
                      <p>Max Upload Speed: <strong>{s.data.Max_ul_spd_Mbits} Mbit/s</strong></p>
                      <p>Avg Upload Speed: <strong>{s.data.Avg_ul_spd_Mbits} Mbit/s</strong></p>
                    </div>
                    <div className='col-xs-12 col-sm-12 col-md-12'>
                      <h4>About this data</h4>
                        <p>The broadband data used on this site if from OFCOMS statistics for <a href="https://www.ofcom.org.uk/research-and-data/telecoms-research/broadband-research/uk-home-broadband-performance-2016?SQ_VARIATION_100598=0" target="_blank">UK home broadband performance</a></p>
                    </div>
                  </div>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

module.exports = Broadband;
