var React = require('react');
var ReactDom = require('react-dom');

class About extends React.Component{
  render(){
    return(
      <div className='container'>
        <div className='row'>
        <div className='col-xs-12'>
          <div className='panel panel-default'>
            <div className='panel-body'>
              <h1>About This App</h1>
              <p>This app is a personal project built to explore ReactJS, and to allow mobile users to access the datasets used on my <a href='https://map.snowmeister.co.uk' target='_blank'>SnowMap</a> project</p>
              <h2>The stack</h2>
              <p>The entire app is written with Javascript and the ReactJS framework, with only one server-side script to act as a proxy to allow loading of data from remote sources where CORS is not an option, and is running on Apache2, on a DigitalOcean Ubuntu Server droplet, and uses the MySQL database from the Snowmap for some of the content (Broadband and Land Registry)</p>
                <h3>APIs &amp; Data Used</h3>
                <ul className="list list-unstyled">
                  <li><a target='_blank' href='https://data.police.uk/docs/'>UK Police API</a>for Street Level Crime data</li>
                  <li><a target='_blank' href='https://postcodes.io/'>Postcodes.io API</a> for Lat/Lng to Postcode Geocoding</li>
                  <li><a target='_blank' href='https://courttribunalfinder.service.gov.uk/api.html'>Courtfinder's API</a> to look up Courts and Tribunals.</li>
                  <li>Various datasets from the <a href='https://data.gov.uk/data/search'>Data.gov.uk</a> open datasets, including UK Schools, and Broadband speeds.</li>
                </ul>
        </div>
          </div>
        </div>
        </div>
      </div>
    )
  }
}
module.exports = About;
