var React = require('react');
var ReactDOM = require('react-dom');
var NavLink = require('react-router-dom').NavLink;
var queryString = require('query-string');
var Redirect = require('react-router-dom').Redirect;
var PropTypes = require('prop-types');


class MainMenuItem extends React.Component {
  render(){
    return (
      <div className='col-xs-12 col-sm-4 col-md-3 col-lg-3 text-center'>
        <div className='menu-item'>
        <NavLink className='menu-item-link' to={this.props.link}><i className={'fa ' + this.props.icon +' fa-fw menu-icon'}></i>{this.props.label}
        </NavLink>
      </div>
      </div>
    )
  }
}

 MainMenuItem.propTypes = {
   'link': PropTypes.string.isRequired,
   'icon': PropTypes.string.isRequired,
   'label': PropTypes.string.isRequired
 }



class MainMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lat: null,
      lng: null,
      postcode: null
    }
    this.resetLocation = this.resetLocation.bind(this);
    this.updateLocationStorage = this.updateLocationStorage.bind(this);
  }

  resetLocation() {
    localStorage.setItem('snowmapdata.lat', null);
    localStorage.setItem('snowmapdata.lng', null);
    localStorage.setItem('snowmapdata.postcode', null);
    return window.location.href = '/';
  }
  updateLocationStorage(lat, lng, postcode) {
    localStorage.setItem('snowmapdata.lat', lat);
    localStorage.setItem('snowmapdata.lng', lng);
    localStorage.setItem('snowmapdata.postcode', postcode);
    this.setState(function() {
      return {lat: lat, lng: lng, postcode: postcode}
    })
    return
  }
  componentDidMount() {
    var qs = queryString.parse(this.props.location.search);
    var lsLat = localStorage.getItem('snowmapdata.lat');
    var lsLng = localStorage.getItem('snowmapdata.lng');
    var lspostcode = localStorage.getItem('snowmapdata.postcode');
    if ((!qs.lat) && (!qs.lng) && (!qs.postcode) && (lsLat === 'null') && (lsLng === 'null') && (lspostcode === 'null')) {
      return window.location.href = '/';
    } else if ((qs.lat) && (qs.lng) && (qs.postcode) && (lsLat === 'null') && (lsLng === 'null') && (lspostcode === 'null')) {
      return this.updateLocationStorage(qs.lat, qs.lng, qs.postcode)
    } else if ((!qs.lat) && (!qs.lng) && (!qs.postcode) && (lsLat !== 'null') && (lsLng !== 'null') && (lspostcode !== 'null')) {
      return this.updateLocationStorage(lsLat, lsLng, lspostcode)
    } else{
      return this.updateLocationStorage(qs.lat, qs.lng, qs.postcode)
    }
  }
  render() {
    var query = '?lat=' + this.state.lat + '&lng=' + this.state.lng + '&postcode=' + this.state.postcode;

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-xs-12'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h1>Main Menu</h1>
                <div className='menu-box'>
                  <MainMenuItem link={'/crime' + query} label='Crime' icon='fa-exclamation-triangle'/>
                  <MainMenuItem link={'/broadband' + query} label='Broadband' icon='fa-wifi'/>
                  <MainMenuItem link={'/courts' + query} label='Courts' icon='fa-gavel'/>
                  <MainMenuItem link={'/schools' + query} label='Schools' icon='fa-mortar-board'/>
                  <MainMenuItem link={'/pricepaid' + query} label='Land Registry' icon='fa fa-home fa-fw'/>





                  <div className='col-xs-12 col-sm-4 col-md-3 col-lg-3 text-center'>
                    <div className='menu-item'>
                    <a onClick={this.resetLocation} className='menu-item-link'>
                      <i className='fa fa-refresh fa-fw menu-icon'></i>
                      Reset Location</a>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
module.exports = MainMenu
