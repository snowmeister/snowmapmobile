//getNearbyPostCodes
var React = require('react');
var PropTypes = require('prop-types');
var Loading = require('../Loading');
var queryString = require('query-string');
var uuidv1 = require('uuid/v1');
var data = require('../../utils/Data');
var postcodes = require('../../utils/Postcodes');
var stringUtils = require('../../utils/Strings');
var locationUtils = require('../../utils/Location');
var dateUtils = require('../../utils/Date');
var Alert = require('../Alert');


class PropertyAgeLabel extends React.Component {
 render(){
   if(this.props.age === 'Y'){
     return (<span className='new'>New</span>)
   }else{
     return (<span className='old'>Old</span>)
   }
 }
}
PropertyAgeLabel.propTypes = {
  age: PropTypes.string.isRequired
}
class FreeholdLabel extends React.Component {
 render(){
   if(this.props.leasetype === 'F'){
     return (<span className='freehold'>Freehold</span>)
   }else{
     return (<span className='leasehold'>Leasehold</span>)
   }
 }
}
FreeholdLabel.propTypes = {
  leasetype: PropTypes.string.isRequired
}
// D = Detached, S = Semi-Detached, T = Terraced, F = Flats/Maisonettes, O = Other
class PropertyTypeLabel extends React.Component {
 render(){
   if(this.props.propertyType === 'S'){
     return (<span className='semi'>Semi-detached</span>)
   }else if(this.props.propertyType === 'F'){
     return (<span className='flat'>Flat/Maisonette</span>)
   }else if(this.props.propertyType === 'D'){
     return (<span className='detached'>Detached</span>)
   }else if(this.props.propertyType === 'T'){
     return (<span className='terraced'>Terraced</span>)
   }else if(this.props.propertyType === 'O'){
     return (<span className='other'>Other</span>)
   }else{
     return null
   }
 }
}
PropertyTypeLabel.propTypes = {
  propertyType: PropTypes.string.isRequired
}

class PricePaid extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      postcodes: [],
      ppdata: [],
      loading: true
    }
  }
  componentDidMount() {
    return postcodes.getNearbyPostCodes().then(function(postcodes) {
      return data.getPPData(postcodes).then(function(data) {
        console.dir(data);
        return this.setState(function() {
          return {postcodes: postcodes, ppdata: data, loading: false}
        })
      }.bind(this))
    }.bind(this))
  }
  render() {
    var s = this.state;
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-xs-12'>
            <div className='panel panel-default'>
              <div className='panel-body'>
                <h1>Land Registry Price Paid</h1>
                {s.loading && <Loading/>}
                {!s.loading &&
                  <div>
                    <Alert alertType='alert-info' title='Coming soon'>
                      <p>This table is just an early part of the work currently in progress, which will include pretty charts for price tracking etc. Check back soon if you'd like to see this area of the app grow.</p>
                    </Alert>
                  <table className='table table-striped table-responsive'>
                    <thead>
                      <tr>
                        <th>Address</th>
                        <th>Price</th>
                        <th>Tenure</th>
                        <th>Type</th>
                        <th>Old/New</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      {s.ppdata.map(function(ppitem){
                        return (<tr key={ppitem.tui}>
                          <td>{ppitem.paon} {stringUtils.toProperCase(ppitem.street.split(' '))}</td>
                          <td>£{stringUtils.numberWithCommas(ppitem.price)}</td>
                          <td><FreeholdLabel leasetype={ppitem.duration} /></td>
                          <td><PropertyTypeLabel propertyType={ppitem.propertyType} /></td>
                          <td><PropertyAgeLabel age={ppitem.oldNew} /></td>
                          <td>{dateUtils.prettifyPPDate(ppitem.dateOfTransfer)}</td>
                        </tr>)
                      })}
                    </tbody>
                  </table>
                  </div>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
PricePaid.propTypes = {}

module.exports = PricePaid;
