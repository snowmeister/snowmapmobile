var React = require('react');

class Loading extends React.Component {
  render(){
    return(
      <div className="alert alert-info">
        <i className="fa fa-spinner fa-spin"></i> Just mo, loading data...
      </div>
    )
  }
}
module.exports = Loading
