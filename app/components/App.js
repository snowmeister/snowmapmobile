// Dependencies
var React = require('react')
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;
// My components
var Nav = require('./Nav');
var Home = require('./Home');
var About = require('./About');
var Broadband = require('./broadband/Broadband');
var Main = require('./MainMenu');
var Courts = require('./courts/Courts');
var Crime = require('./crime/Crime');
var Schools = require('./schools/Schools');
var PricePaid = require('./pricepaid/PricePaid');
var Alert = require('./Alert');
class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      location: null
    }
  }
  componentDidMount(){
    var lsLat = localStorage.getItem('snowmapdata.lat');
    var lsLng = localStorage.getItem('snowmapdata.lng');
    var lspostcode = localStorage.getItem('snowmapdata.postcode');
    if((lsLat === null) && (lsLng === null)){
      // We need to set the localStorage so we can preseve the users location
      localStorage.setItem('snowmapdata.lat', null);
      localStorage.setItem('snowmapdata.lng', null);
      localStorage.setItem('snowmapdata.postcode', null);
    }
  }
  render(){
    return (
      <Router>
        <div className=''>
            <Nav />
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/main' component={Main} />
                <Route path='/about' component={About} />
                <Route path='/broadband' component={Broadband} />
                <Route path='/schools' component={Schools} />
                <Route path='/courts' component={Courts} />
                <Route path='/crime' component={Crime} />
                <Route path='/pricepaid' component={PricePaid} />
                <Route render={function(){
                  return (
                    <div className='container'>
                      <div className='row'>
                      <div className='col-xs-12'>
                        <div className='panel panel-default'>
                          <div className='panel-body'>
                          <Alert title='Not Found' alertType='alert-warning'>
                            <p>Oops. This is a 404! It seems you are trying to load a page that does not exist.</p>
                          </Alert>
                        </div>
                      </div>
                    </div>
                    </div>
                    </div>
                  )
                }} />
            </Switch>
        </div>
      </Router>
    )
  }
}
module.exports = App;
