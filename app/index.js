// We need React and ReactDom to work the magic!
var React = require('react');
var ReactDOM = require('react-dom');
// The main APP Component
var App = require('./components/App');

// Lets not forget our css
import styles from './index.css';

//Now lets render it all out...
ReactDOM.render(
  <App name="snowmapmobile"/>,
  document.getElementById('app')
);
