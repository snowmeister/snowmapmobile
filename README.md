# SnowMapMobile

This is a personal project, built to explore ReactJS, which I am hoping will demonstrate to potential employers that I am able to work productively with it,and as I intend to provide a "read world" demo of my current ReactJS skillset I intend to provide some of the [Snowmap](https://bitbucket.org/snowmeister/snowmap) functionality to mobile devices, WITHOUT maps.

If you are interested in getting stuck into React, I can highly recommend [Tyler McGinnis' React Fundamentals course](https://learn.tylermcginnis.com/courses/)

#Demo

The work-in-progess of this code can be seen running [here](https://aroundme.snowmeister.co.uk)

## Requirements

You will need Node and NPM installed on your dev machine to be able to build this code.

## Getting started

1. Clone this repo
1. CD into the repo directory
1. npm install
